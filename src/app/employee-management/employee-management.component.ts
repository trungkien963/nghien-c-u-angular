import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { employees } from '../../data-local/employees';
import { EmployeeModel } from '../../models/employee_model';
import { AddEmployeeComponent } from './add-employee/add-employee.component';
import { DetailEmployeeComponent } from './detail-employee/detail-employee.component';
import swal from 'sweetalert2';
import { EmployeeService } from 'app/services/employee.service';

@Component({
  selector: 'app-employee-management',
  templateUrl: './employee-management.component.html',
  styleUrls: ['./employee-management.component.css']
})
export class EmployeeManagementComponent implements OnInit {

  employees: EmployeeModel[];

  constructor(public dialog: MatDialog, public employeeService: EmployeeService) { }

  ngOnInit(): void {
    this.getData();
  }

  addEmployee() {
    let dialogRef = this.dialog.open(AddEmployeeComponent, {
      panelClass: "add-employee",
      width: "80vw",
      height: "75vh"
    });

    dialogRef.afterClosed().subscribe(res => {
      this.getData();
    })
  }

  gotoEditEmployee(isEdit: boolean, employee: EmployeeModel) {
    let dialogRef = this.dialog.open(DetailEmployeeComponent, {
      panelClass: "add-employee",
      width: "80vw",
      height: "75vh",
      data: {
        employee: employee,
        isEdit: isEdit
      }
    });
    dialogRef.afterClosed().subscribe(res => {
      this.getData();
    })
  }

  preDeleteEmployee(item: EmployeeModel) {
    swal.fire({
      title: 'Xác nhận xóa!',
      text: `Bạn có chắc xóa ${item.name}`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Đồng ý',
      cancelButtonText: 'Hủy',
      customClass: {
        confirmButton: "btn btn-success",
        cancelButton: "btn btn-danger",
      },
      buttonsStyling: false
    }).then((result) => {
      console.log(result);

      if (result.value) {
        this.deleteEmployee(item);
      } else {
      }
    })

  }

  deleteEmployee(item: EmployeeModel) {
    console.log(item);

    this.employeeService.deleteEmployee(item).subscribe((res) => {
      this.getData();
    })
  }

  getData() {
    this.employeeService.getListEmployee().subscribe((res: EmployeeModel[]) => {
      this.employees = res;
    })
  }

}
