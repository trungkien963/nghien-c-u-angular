import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SpecialValidator } from '../../helpers/except_special.validator';
import { employees } from '../../../data-local/employees';
import { EmployeeModel } from '../../../models/employee_model';
import { ImageSnippet } from '../../../models/image_snippet';
import { AddEmployeeComponent } from '../add-employee/add-employee.component';
import * as moment from 'moment';
import { EmployeeService } from 'app/services/employee.service';

@Component({
  selector: 'app-detail-employee',
  templateUrl: './detail-employee.component.html',
  styleUrls: ['./detail-employee.component.css']
})
export class DetailEmployeeComponent implements OnInit {

  isEdit = false;
  loading = false;

  registerForm!: FormGroup;
  submitted = false;

  img = "https://huyhoanhotel.com/wp-content/uploads/2016/05/765-default-avatar.png";
  isChangeAvatar = false;
  selectedFile!: ImageSnippet;

  constructor(
    private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<AddEmployeeComponent>,
    public employeeService: EmployeeService,
    @Inject(MAT_DIALOG_DATA) public data: { employee: EmployeeModel, isEdit: boolean }
  ) { }

  ngOnInit(): void {
    this.registerForm = this.formBuilder.group({
      id: [{ value: this.data.employee.id, disabled: !this.data.isEdit }, Validators.required],
      name: [{ value: this.data.employee.name, disabled: !this.data.isEdit }, Validators.required],
      address: [{ value: this.data.employee.address, disabled: !this.data.isEdit }, Validators.required],
      birth: [{ value: moment(this.data.employee.birth, "DD/MM/YYYY").format("YYYY-MM-DD"), disabled: !this.data.isEdit }, Validators.required],
      code: [{ value: this.data.employee.code, disabled: !this.data.isEdit }, Validators.required],
      image: [{ value: this.data.employee.image, disabled: !this.data.isEdit }, Validators.required],
      email: [{ value: this.data.employee.email, disabled: !this.data.isEdit }, [Validators.required, Validators.email]]
    }, {
      validator: SpecialValidator('code')
    });

    this.img = this.data.employee.image;

    this.isEdit = this.data.isEdit;
  }

  get f() { return this.registerForm.controls; }

  onSubmit() {
    this.submitted = true;
    this.loading = true;

    if (this.registerForm.invalid) {
      return;
    }

    let employee: EmployeeModel;

    employee = Object.assign(this.registerForm.value, { image: this.img });

    employee.birth = moment(employee.birth, "YYYY-MM-DD").format("DD/MM/YYYY");

    this.employeeService.updateEmployee(employee).subscribe(res => {
      this.loading = false;
      console.log(res);
      this.cancel();
    });


  }

  cancel() {
    this.dialogRef.close();
  }

  processFile(imageInput: any) {
    console.log(imageInput);
    const file: File = imageInput.files[0];
    const reader = new FileReader();

    reader.addEventListener('load', (event: any) => {
      console.log(event);

      this.selectedFile = new ImageSnippet(event.target.result, file);

      this.img = this.selectedFile.src;
      this.isChangeAvatar = true;
    });

    reader.readAsDataURL(file);
  }

}