import { Routes } from "@angular/router";
import { EmployeeManagementComponent } from "./employee-management.component";

export const EmployeeManagementRoutes: Routes = [
    {
        path: '',
        children: [{
            path: '',
            component: EmployeeManagementComponent
        }]
    }
];