import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { SpecialValidator } from '../../helpers/except_special.validator';
import { EmployeeModel, EmployeeViewModel } from '../../../models/employee_model';
import { ImageSnippet } from '../../../models/image_snippet';
import { employees } from '../../../data-local/employees';
import * as moment from 'moment';
import { EmployeeService } from 'app/services/employee.service';

@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.css']
})
export class AddEmployeeComponent implements OnInit {

  registerForm!: FormGroup;
  submitted = false;
  loading = false;

  img = "https://huyhoanhotel.com/wp-content/uploads/2016/05/765-default-avatar.png";
  isChangeAvatar = false;
  selectedFile!: ImageSnippet;

  constructor(private formBuilder: FormBuilder, public employeeService: EmployeeService, public dialogRef: MatDialogRef<AddEmployeeComponent>) { }

  ngOnInit(): void {
    this.registerForm = this.formBuilder.group({
      name: ['', Validators.required],
      address: ['', Validators.required],
      birth: ['', Validators.required],
      code: ['', Validators.required],
      image: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]]
    }, {
      validator: SpecialValidator('code')
    });
  }

  get f() { return this.registerForm.controls; }

  onSubmit() {
    this.loading = true;

    console.log(this.registerForm.value);

    this.submitted = true;

    if (this.registerForm.invalid) {
      return;
    }

    let employee: EmployeeViewModel;

    employee = this.registerForm.value;

    employee.birth = moment(employee.birth, "YYYY-MM-DD").format("DD/MM/YYYY");

    this.employeeService.createEmployee(employee).subscribe(res => {
      this.loading = false;
      this.cancel();
    })



  }

  cancel() {
    this.dialogRef.close();
  }

  processFile(imageInput: any) {
    console.log(imageInput);
    const file: File = imageInput.files[0];
    const reader = new FileReader();

    reader.addEventListener('load', (event: any) => {
      console.log(event);

      this.selectedFile = new ImageSnippet(event.target.result, file);

      this.img = this.selectedFile.src;
      this.isChangeAvatar = true;
    });

    reader.readAsDataURL(file);
  }

}
