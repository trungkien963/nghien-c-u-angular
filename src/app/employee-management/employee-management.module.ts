import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmployeeManagementComponent } from './employee-management.component';
import { EmployeeManagementRoutes } from './employee-management.routing';
import { RouterModule } from '@angular/router';
import { MaterialModule } from '../app.module';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { AddEmployeeComponent } from './add-employee/add-employee.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DetailEmployeeComponent } from './detail-employee/detail-employee.component';



@NgModule({
  declarations: [EmployeeManagementComponent, AddEmployeeComponent, DetailEmployeeComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(EmployeeManagementRoutes),
    MaterialModule,
    MatDialogModule,
    MatButtonModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class EmployeeManagementModule { }
