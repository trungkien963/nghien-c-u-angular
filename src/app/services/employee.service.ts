import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiUrl } from 'app/helpers/API_config';
import { map } from 'rxjs/operators';
import { EmployeeModel, EmployeeViewModel } from '../../models/employee_model';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor(private http: HttpClient) { }

  getListEmployee() {
    let URL: string = ApiUrl.urlEmployee;
    let headers = {};

    return this.http.get(URL).pipe(map((res:any) => {

      let listEmployee: EmployeeModel[] = res;

      return listEmployee;
    }));
  }

  getEmployeeByID(id: string) {
    let URL: string = ApiUrl.urlEmployee + id;

    return this.http.get(URL);
  }

  createEmployee(employee: EmployeeViewModel) {
    let URL: string = ApiUrl.urlEmployee;

    return this.http.post(URL, employee);
  }

  updateEmployee(employee: EmployeeModel) {
    let URL: string = ApiUrl.urlEmployee + employee.id;
    return this.http.put(URL, employee);
  }

  deleteEmployee(employee: EmployeeModel) {

    console.log(employee);
    let URL: string = ApiUrl.urlEmployee + employee.id;

    return this.http.delete(URL);
  }
}
