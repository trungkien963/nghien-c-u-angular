import { environment } from '../../environments/environment';

class ApiUrl {
    static domain = environment.ServiceConfig;

    //employee
    static urlEmployee= ApiUrl.domain +"/api/employee/"
}

export {
    ApiUrl
}