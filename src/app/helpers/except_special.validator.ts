import { FormGroup } from "@angular/forms";

export function SpecialValidator(controlName: string) {
    return (formGroup: FormGroup) => {
        const control = formGroup.controls[controlName];

        if (control.errors && !control.errors.specialString) {
            return;
        }

        if (checkSpecialCharacter(control.value)) {
            control.setErrors({ specialString: true });
        } else {
            control.setErrors(null);
        }
    }
}

function checkSpecialCharacter(str: string): boolean {

    let reg = /[!@#$%^&*(),`~'.?":;{}\\+/=\-_|<>]/g;

    return reg.test(str);
}
