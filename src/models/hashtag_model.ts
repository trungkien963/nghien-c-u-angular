export interface HashtagModel {
    id: number,
    status: number,
    type: number,
    value: string
}

export class Human {
    name: string;
    age: Number;

    constructor(name: string, age: Number) {
        this.name = name;
        this.age = age;
    }

    say() {
        console.log("hello, my name is: " + this.name, "and my age: " + this.age);
    }

    goodbye() { }
}

export class AsianHuman extends Human {
    skincolor: string;

    constructor(skincolor: string, name: string, age: Number) {
        super(name, age);
        this.skincolor = skincolor;
    }

    sayHello() {
        super.say();

        console.log("my skin color is: ", this.skincolor);
    }

    goodbdye() {
        console.log("GOODBYE");
    }
}

export class AsiaHuman implements Human{
    name: string;
    age: Number;

    constructor(name: string, age: Number) {
        this.name = name;
        this.age = age;
    }

    say(): void {
        console.log("Say Hello.");
    }
    goodbye(): void {
        console.log("Say goodbye.");
    }
    
}
