export interface EmployeeModel {
    id: number,
    name: string,
    code: string,
    address: string,
    birth: string,
    email: string,
    image: string,
}

export interface EmployeeViewModel {
    name: string,
    code: string,
    address: string,
    birth: string,
    email: string,
    image: string,
}