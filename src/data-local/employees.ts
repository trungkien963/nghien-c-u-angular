import { EmployeeModel } from "../models/employee_model";

export let employees: EmployeeModel[] = [
    {
        id: 1,
        name: "Phạm Trung Kiên",
        code: "A123",
        address: "42 Phạm Ngọc Thạch",
        birth: "01/01/1998",
        email: "kien@gmail.com",
        image: "https://huyhoanhotel.com/wp-content/uploads/2016/05/765-default-avatar.png"
    },
    {
        id: 2,
        name: "Nguyễn Văn A",
        code: "B111",
        address: "13 Điện Biên Phủ",
        birth: "09/09/1997",
        email: "vana@gmail.com",
        image: "https://huyhoanhotel.com/wp-content/uploads/2016/05/765-default-avatar.png"
    },
    {
        id: 3,
        name: "Nguyễn Trà Mi",
        code: "B928",
        address: "748 Hai Bà Trưng",
        birth: "09/03/2001",
        email: "trami@gmail.com",
        image: "https://huyhoanhotel.com/wp-content/uploads/2016/05/765-default-avatar.png"
    },
    {
        id: 4,
        name: "Huỳnh Lam Phương",
        code: "LP302",
        address: "79 Võ Thị Sáu",
        birth: "05/04/2002",
        email: "phuong@gmail.com",
        image: "https://huyhoanhotel.com/wp-content/uploads/2016/05/765-default-avatar.png"
    },
    {
        id: 5,
        name: "Bành Vũ Yến",
        code: "VY102",
        address: "123 Tô Ngọc Vân",
        birth: "09/02/1998",
        email: "vuyen@gmail.com",
        image: "https://huyhoanhotel.com/wp-content/uploads/2016/05/765-default-avatar.png"
    },
    {
        id: 6,
        name: "Hồ Ngọc Hà",
        code: "HNH1646",
        address: "24 Phan Văn Trị",
        birth: "05/11/1987",
        email: "hnh@gmail.com",
        image: "https://huyhoanhotel.com/wp-content/uploads/2016/05/765-default-avatar.png"
    },
    {
        id: 7,
        name: "Angelina Jolie",
        code: "AJ739",
        address: "12 Tú Xương",
        birth: "04/06/1975",
        email: "aj@gmail.com",
        image: "https://huyhoanhotel.com/wp-content/uploads/2016/05/765-default-avatar.png"
    },
    {
        id: 8,
        name: "William Bradley Pitt",
        code: "BP0012",
        address: "73 Hồ Văn Huê",
        birth: "18/12/1963",
        email: "bp@gmail.com",
        image: "https://huyhoanhotel.com/wp-content/uploads/2016/05/765-default-avatar.png"
    },
    {
        id: 9,
        name: "Taylor Swift",
        code: "TS293",
        address: "23 Phan Đình Phùng",
        birth: "13/12/1989",
        email: "ts@gmail.com",
        image: "https://huyhoanhotel.com/wp-content/uploads/2016/05/765-default-avatar.png"
    },
]